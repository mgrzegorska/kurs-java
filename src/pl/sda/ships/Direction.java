package pl.sda.ships;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
