package pl.sda.ships;

public enum GameBoard {
    FILED_EMPTY,
    MISSED,
    AUTO_EMPTY,
    SHIP,
    SHIP_HITED,
    SHIP_SUNK,
    SHOT_TAKEN
}
