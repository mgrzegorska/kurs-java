package pl.sda.ships;

public enum ShotResult {
    MISSED,
    HIT,
    HIT_AND_SUNK
}
